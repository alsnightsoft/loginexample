package edu.pucmm.routes;

import edu.pucmm.model.Item;
import edu.pucmm.model.RouteBase;
import edu.pucmm.utils.Constants;
import edu.pucmm.utils.DataCreator;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import spark.Request;
import spark.Response;

import java.io.StringWriter;

public class Store extends RouteBase {

    public Store() {
        super("/store");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        Template template = velocityEngine.getTemplate("html/store.html");
        VelocityContext context = new VelocityContext();
        context.put("items", DataCreator.readData(Constants.DATA_FILE, Item.class));
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }
}
