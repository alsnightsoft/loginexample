package edu.pucmm.routes;

import edu.pucmm.model.RouteBase;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import spark.Request;
import spark.Response;

import java.io.StringWriter;

/**
 * @author a.marte
 */
public class Login extends RouteBase {

    public Login() {
        super("/login");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        Template template = velocityEngine.getTemplate("html/login.html");
        VelocityContext context = new VelocityContext();
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }

}
