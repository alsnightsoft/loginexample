package edu.pucmm.routes;

import edu.pucmm.model.RouteBase;
import edu.pucmm.utils.Constants;
import spark.Request;
import spark.Response;

/**
 * @author a.marte
 */
public class Logout extends RouteBase {

    public Logout() {
        super("/logout");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        response.cookie("/", Constants.COOKIE_NAME, Constants.COOKIE_FALSE, 3600, false, true);
        return "1";
    }

}
